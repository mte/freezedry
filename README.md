# freezeDRY
`freezeDRY` is a caching framework. It speeds up the development and running of your code and encourages a multi source file project structure.

## How it works
Your client code calls the `freezeDRY` function to perform some long running calculation *X*. As part of the call, you let `freezeDRY` know how long the
results can safely be cached, and which other cached results (prerequisites) are needed.  `freezeDRY` checks to see if there is a previously
cached result (in file *X*.RDS) that is recent enough for your requirements and newer than the prerequisites.  If there is such a result, it is
loaded from the cache and returned to your client code.  This is a great result, because in terms of development and running time, it is 
"lightening fast". If there is not an acceptable cached result, `freezeDRY` does the following actions - 

1. If *X*.SQL exists in the current working directory, it is run on the default database and the results stored in a tibble named *X*.
2. If *X*.R does not exist in the current working directory, the tibble *X* is returned to your client code as the result.
3. if *X*.R does exist in the current working directory, it is run. *X*.R is essentially a function body.  It does not declare a name, parameters or a return type - it is just the body of a function.  The tibble *X* is within scope, so you can consider this to be a parameter. If your client code called `freezeDRY` declaring that *A, B* and *C* are prerequisites, these will be loaded and available as variables *A, B* and *C*.  So you can consider these prerequisites as parameters as well.  The code that you write within the function body is intended to complete calculation *X*, store the results into the tibble *X*, cache the result as *X*.RDS and return *X* to your client code. The caching and the return are handled by `freezeDRY` for you. 
**TO DO** If your *X*.R function body makes changes to its prerequisites (*A, B* and *C*), `freezeDRY` will notice this and re-cache them. 
I suggest this is done by using the `address()` function from the `pryr` package. 

## Convention over configuration
`freezeDRY` enforces the convention that your `.SQL` (How it works #1) and function body `.R` (How it works #3) files must have the same basename. 
*X* was the example used in the section above.  This basename should be descriptive of the problem being solved, or the calculation being performed. 
The basename will also be the name of the main parameter to your function body.  Your function body should store the calculation result in that 
parameter variable (preferably as a `tibble`). This will be cached in a `.RDS` file with the same basename, then returned to your client code. 
So provided that you choose one descriptive basename and consistently use it, you don't need to write any *wiring or glue* code.  The `freezeDRY`
framework already knows where to look.

## Compartmentalize your code
When your client code calls `freezeDRY`, it will look for a `.R` file and a `.SQL` file with a basename matching the `id` parameter. There has to 
be at least one of these files.  If both are found, the `.SQL` file is executed first and the results are passed to the `.R` file.  This arrangement
encourages you to divide your client code into groups of related files (`.SQL`, `.R`, `.RDS`), having the same base name, each having a standardized
defined role in the calculate / cache workflow and (*ideally*) dealling with one step in sequence of interrelated calculations.


